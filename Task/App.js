
import React, { Component } from 'react';

import { View, Image, TouchableOpacity, StyleSheet, Text, Dimensions, ImageBackground, TouchableHighlight } from 'react-native';
import { Container, Content, Icon} from 'native-base'

import {
  createDrawerNavigator,
  createStackNavigator,
  createAppContainer, DrawerItems
} from 'react-navigation';

import Screen1 from './src/screens/Screen1';
import Screen2 from './src/screens/Screen2';
import Screen3 from './src/screens/Screen3';
import Screen4 from './src/screens/Screen4';
import Screen5 from './src/screens/Screen5';
import Screen6 from './src/screens/Screen6';
import Screen7 from './src/screens/Screen7';

class NavigationDrawerStructure extends Component {

  toggleDrawer = () => {

    this.props.navigationProps.toggleDrawer();
  };
  render() {
    return (
      <View style={styles.flexRow}>



        <TouchableOpacity onPress={this.toggleDrawer.bind(this)}>

          <Image
            source={require('./src/img/drawer.png')}
            style={styles.navigatorDrawerImageStyle}
          />

        </TouchableOpacity>
      </View>
    );
  }
}

const CustomDrawerContentComponent = (props) => (


  <View style={styles.flexOne}>

    <View style={styles.flexOne} >
      <ImageBackground source={require('./src/img/background.png')} imageStyle={{ resizeMode: 'cover' }} style={[styles.flexRow, { width: '100%', height: '100%' }]}>
        <View style={styles.flexHalf} />
        <View style={styles.flexOne} >
          <Image
            style={styles.drawerImage}
            source={require('./src/img/logo.png')} />
        </View>

        <View style={styles.flexFour} />
        <View style={[styles.flexHalf, { flexDirection: 'column' }]} >
          <View style={styles.flexHalf} />
          <View style={styles.flexOne} >
            <Icon style={{ color: 'grey' }} name="close"></Icon>
          </View>
          <View style={styles.flexOne} />
        </View>

      </ImageBackground>
    </View>
    <View style={[styles.flexFour, styles.BackgroundColor]}>
      <DrawerItems

        activeTintColor={"white"}
        inactiveTintColor={"white"}
        labelStyle={styles.drawerItemLabelStyle}


        {...props} />


    </View>
    <View style={[styles.flexHalf, styles.BackgroundColor, { justifyContent: 'space-around' }, styles.flexRow]}>
      <View style={[styles.flexOne, { padding: 5 }]}>

        <TouchableHighlight
          style={styles.submit}
          onPress={() => { console.log("button pressed") }}
          underlayColor='#fff'>
          <Text style={styles.submitText}>Get Appointment</Text>
        </TouchableHighlight>

      </View>
      <View style={[styles.flexOne, { padding: 5 }]}>

        <TouchableHighlight
          style={styles.submit}
          onPress={() => { console.log("button pressed") }}
          underlayColor='#fff'>
          <Text style={styles.submitText}>Call +91 234567890</Text>
        </TouchableHighlight>



      </View>


    </View>
  </View>

);


//Stack navigator for each of the screens
const FirstActivity_StackNavigator = createStackNavigator({

  First: {
    screen: Screen1,
    navigationOptions: ({ navigation }) => ({
      headerTitle: <Text style={styles.headerTextStyle}>Home</Text>,
      headerRight: <NavigationDrawerStructure navigationProps={navigation} />,
      headerStyle: {
        backgroundColor: 'white',
        elevation: 0
      },
      headerTitleStyle: {
        fontFamily: 'bold',
        fontWeight: "200"
      },
      headerTintColor: '#000',
    }),
  },
});


const Screen2_StackNavigator = createStackNavigator({

  Second: {
    screen: Screen2,
    navigationOptions: ({ navigation }) => ({

      headerTitle: <Text style={styles.headerTextStyle}>Our Story</Text>,
      headerRight: <NavigationDrawerStructure navigationProps={navigation} />,
      headerStyle: {
        backgroundColor: 'white',
        elevation: 0,

      },
      headerTintColor: '#000',

    }),
  },
});


const Screen3_StackNavigator = createStackNavigator({

  Third: {
    screen: Screen3,
    navigationOptions: ({ navigation }) => ({
      headerTitle: <Text style={styles.headerTextStyle}>Meet the Doctor</Text>,
      headerRight: <NavigationDrawerStructure navigationProps={navigation} />,
      headerStyle: {
        backgroundColor: 'white',
        elevation: 0
      },

      headerTintColor: '#000',
    }),
  },
});
const Screen4_StackNavigator = createStackNavigator({

  Four: {
    screen: Screen4,
    navigationOptions: ({ navigation }) => ({
      headerTitle: <Text style={styles.headerTextStyle}>Our Services</Text>,
      headerRight: <NavigationDrawerStructure navigationProps={navigation} />,
      headerStyle: {
        backgroundColor: 'white',
        elevation: 0
      },

      headerTintColor: '#000',
    }),
  },
});
const Screen5_StackNavigator = createStackNavigator({

  Five: {
    screen: Screen5,
    navigationOptions: ({ navigation }) => ({
      headerTitle: <Text style={styles.headerTextStyle}>Updates</Text>,
      headerRight: <NavigationDrawerStructure navigationProps={navigation} />,
      headerStyle: {
        backgroundColor: 'white',
        elevation: 0
      },

      headerTintColor: '#000',
    }),
  },
});
const Screen6_StackNavigator = createStackNavigator({

  Six: {
    screen: Screen6,
    navigationOptions: ({ navigation }) => ({
      headerTitle: <Text style={styles.headerTextStyle}>More Information</Text>,
      headerRight: <NavigationDrawerStructure navigationProps={navigation} />,
      headerStyle: {
        backgroundColor: 'white',
        elevation: 0
      },

      headerTintColor: '#000',
    }),
  },
});
const Screen7_StackNavigator = createStackNavigator({

  Seven: {
    screen: Screen7,
    navigationOptions: ({ navigation }) => ({
      headerTitle: <Text style={styles.headerTextStyle}>Contact</Text>,
      headerRight: <NavigationDrawerStructure navigationProps={navigation} />,
      headerStyle: {
        backgroundColor: 'white',
        elevation: 0
      },

      headerTintColor: '#000',
    }),
  },
});


//Drawer Navigator for all screens
const DrawerNavigatorExample = createDrawerNavigator({

  Screen1: {

    screen: FirstActivity_StackNavigator,
    navigationOptions: {
      drawerLabel: 'Home',
    },

  },
  Screen2: {

    screen: Screen2_StackNavigator,
    navigationOptions: {
      drawerLabel: 'Our story',
    },
  },
  Screen3: {

    screen: Screen3_StackNavigator,
    navigationOptions: {
      drawerLabel: 'Meet the doctor',
    },
  },
  Screen4: {

    screen: Screen4_StackNavigator,
    navigationOptions: {
      drawerLabel: 'Our Services',
    },
  },
  Screen5: {

    screen: Screen5_StackNavigator,
    navigationOptions: {
      drawerLabel: 'Updates',
    },
  },
  Screen6: {

    screen: Screen6_StackNavigator,
    navigationOptions: {
      drawerLabel: 'More Information',
    },
  },
  Screen7: {

    screen: Screen7_StackNavigator,
    navigationOptions: {
      drawerLabel: 'Contact',
    },
  },

}, {
    drawerPosition: 'right',
    contentComponent: CustomDrawerContentComponent,
    drawerWidth: Dimensions.get('window').width * 1
  },

);


export default createAppContainer(DrawerNavigatorExample);


const styles = StyleSheet.create({

  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },
  drawerHeader: {
    height: 100,
    backgroundColor: 'white',
    elevation: 0
  },
  drawerImage: {
    height: 100,
    width: 100,
  },
  containerStyle: {
    backgroundColor: '#7db9b9'
  },
  headerTextStyle: {
    textAlign: 'justify',
    flex: 1,
    fontFamily: 'bold',
    fontSize: 25,
    color: '#7db9b9',
    paddingLeft: 10
  },
  submit: {
    padding: 3,
    backgroundColor: '#7db9b9',
    borderRadius: 30,
    borderWidth: 1,
    borderColor: '#fff'
  },
  submitText: {
    color: '#fff',
    textAlign: 'center',
    fontFamily: 'Montserrat'
  },
  flexOne: {
    flex: 1
  },
  flexHalf: {
    flex: 0.5
  },
  flexRow: {
    flexDirection: 'row'
  },
  drawerItemLabelStyle: {
    fontFamily: 'Montserrat',
    fontSize: 20,
    fontWeight: 'bold'
  },
  flexFour: {
    flex: 4
  },
  BackgroundColor: {
    backgroundColor: '#7db9b9'
  },
  navigatorDrawerImageStyle: {
    width: 25,
    height: 25,
    marginRight: 8
  }


})