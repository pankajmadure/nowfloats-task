import React, { Component } from 'react';
import { StyleSheet, View, Text } from 'react-native';


export default class Screen6 extends Component {

  render() {
    return (
      <View style={styles.MainContainer}>
        <Text style={styles.textStyle}> More Information </Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  MainContainer: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#7db9b9'
  },
  textStyle: {
    fontSize: 23
  }
});