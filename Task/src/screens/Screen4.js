import React, { Component } from 'react';
import { StyleSheet, View, FlatList, Image, Text, Button, ToastAndroid } from 'react-native';
import axios from "axios";
import ViewMoreText from 'react-native-view-more-text';

import ElevatedView from 'react-native-elevated-view'


var that;

export default class Services extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: "",
      isLoading: true,
      cardHeight: 400,
      open: false
    }
    that = this;
    this.getData = this.getData.bind(this);
  }



  componentDidMount() {

    this.getData()


  }
  getData() {
    let payload = {
      token: "gij5_Vd4QrJXE5pPO8TJ5g",
      data: {
        name: "nameFirst",
        email: "internetEmail",
        phone: "phoneHome",
        _repeat: 300
      }
    };

    axios({
      method: "get",
      url: "https://app.fakejson.com/q/Q4q534Nv?token=gij5_Vd4QrJXE5pPO8TJ5g",
      data: payload
    }).then(function (resp) {
      // Do something with fake data

      that.setState({
        data: resp.data.personaldata,
        isLoading: false
      })
    });

  }
  _renderViewMore = (handlePress) => {


    return (
      <Text style={styles.viewMoreAndLessStyle} onPress={handlePress}>
        Read more
      </Text>
    );
  }

  _renderViewLess = (handlePress) => {
    return (
      <Text style={styles.viewMoreAndLessStyle} onPress={handlePress}>
        Show less
      </Text>
    );
  }
  afterExpand() {
    console.log("expanded")
  }

  render() {
    if (this.state.isLoading) {
      return (
        <View>
          <Text>Loading.......</Text>
        </View>
      )

    }
    else {
      return (
        <View style={styles.MainContainer}>
          <FlatList

            data={this.state.data}
            ItemSeparatorComponent={() => (

              <View style={styles.itemSeperator} />

            )}
            renderItem={({ item }) => {
              return (
                <View style={styles.viewStyle}>
                  <ElevatedView
                    elevation={9}
                    style={styles.elevationStyle}
                  >
                    <View style={[styles.viewStyle, styles.viewWidthHeightStyle]}>
                      <View style={styles.viewStyle}>
                        <Image style={styles.imageStyle} source={{ uri: item.image }}></Image>


                      </View>
                      <View style={[styles.viewStyle, { paddingLeft: 15, paddingRight: 20 }]}>

                        <Text style={styles.itemNameStyle}>{item.name}</Text>
                        <Text style={styles.itemPriceStyle}>INR {item.price}</Text>
                        <ViewMoreText
                          numberOfLines={2}
                          key={item}
                          renderViewMore={this._renderViewMore}
                          renderViewLess={this._renderViewLess}
                        >
                          <Text style={styles.itemDescriptionStyle}>{item.description}</Text>

                        </ViewMoreText>


                        <Button color={'#7db9b9'} title="BOOK APPOINTMENT" onPress={() => {

                          ToastAndroid.show('BOOK APPOINTMENT is pressed', ToastAndroid.SHORT);


                        }}></Button>

                      </View>
                    </View >

                  </ElevatedView >
                </View>
              )

            }

            }

            keyExtractor={item => item.image}

          />
        </View>
      );
    }



  }
}

const styles = StyleSheet.create({
  MainContainer: {
    flex: 1,
    paddingLeft: 20,
    paddingRight: 25,
    backgroundColor: '#7db9b9'
  },
  textStyle: {
    fontSize: 23
  },
  itemSeperator: {

    height: 10
  },
  viewMoreAndLessStyle: {
    color: "#7db9b9",
    fontSize: 15,
    marginTop: 3,
    marginBottom: 3
  },
  elevationStyle: {
    width: '100%',
    height: "100%",
    margin: 10,
    backgroundColor: 'white'
  },
  viewStyle: {
    flex: 1
  },
  viewWidthHeightStyle: {
    width: "100%",
    height: "100%",
  },
  imageStyle: {
    width: "100%",
    height: 300,
    resizeMode: 'cover'
  },
  itemNameStyle: {
    fontSize: 25,
    color: 'black',
    fontFamily: 'Montserrat',
    fontWeight: 'bold'
  },
  itemPriceStyle: {
    color: "#7db9b9",
    fontSize: 17,
    fontFamily: 'Montserrat',
    fontWeight: 'bold'
  },
  itemDescriptionStyle: {
    fontSize: 15,
    color: 'black',
    fontFamily: 'Montserrat'
  }

});
