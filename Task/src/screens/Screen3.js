import React, { Component } from 'react';
import { StyleSheet, View, Text } from 'react-native';


export default class Screen3 extends Component {

  render() {
    return (
      <View style={styles.MainContainer}>
        <Text style={styles.textStyle}> Meet the Doctor</Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  MainContainer: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#7db9b9'
  },
  textStyle: {
    fontSize: 23
  }
});